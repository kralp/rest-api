package cz.enehano.training.demoapp.restapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Email;



@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Email
    @Column(unique = true, nullable = false)
    private String email;

    private String phoneNr;

    @Column(nullable = false)
    private String password;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    @JsonIgnoreProperties
    private long createdDate;

    @JsonIgnoreProperties
    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;


    public User(String firstName, String lastName, String email, String phoneNr, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNr = phoneNr;
        this.password = password;
    }


    public User(User user) {
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.phoneNr = user.phoneNr;
        this.email = user.email;
        this.password = user.password;
    }

    public User() {
        super();
    }


}

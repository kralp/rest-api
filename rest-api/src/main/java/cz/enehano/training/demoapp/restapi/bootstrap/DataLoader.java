package cz.enehano.training.demoapp.restapi.bootstrap;

import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private UserService userService;


    private Logger logger = Logger.getLogger("DataLoader.class");

    @Autowired
    public DataLoader(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        createUsers();

    }

    public void createUsers() {
        User user = new User();
        user.setFirstName("admin");
        user.setLastName("admin");
        user.setEmail("admin@admin.cz");
        user.setPhoneNr("12345677");
        user.setPassword("password");
        userService.createUser(user);

        user = new User();
        user.setFirstName("user");
        user.setLastName("user");
        user.setEmail("user@user.cz");
        user.setPhoneNr("89790870987");
        user.setPassword("password");
        userService.createUser(user);

        user = new User();
        user.setFirstName("pavel");
        user.setLastName("kral");
        user.setEmail("pavel@pkral.cz");
        user.setPhoneNr("8978788989889");
        user.setPassword("password");
        userService.createUser(user);

        user = new User();
        user.setFirstName("jakub");
        user.setLastName("novak");
        user.setEmail("jakub@novak.cz");
        user.setPhoneNr("1234fdadfasdfa");
        user.setPassword("password");
        userService.createUser(user);

    }
}

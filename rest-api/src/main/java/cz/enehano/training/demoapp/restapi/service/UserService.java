package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.dao.UserRepository;
import cz.enehano.training.demoapp.restapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    public List<User> getAllUsers() {
        return userRepository.findAllByOrderByLastName();
    }

    @Secured("ROLE_USER")
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    @Secured("ROLE_USER")
    public Optional<User> getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Secured("ROLE_USER")
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Secured("ROLE_USER")
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}

package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.model.UserNotFoundException;
import cz.enehano.training.demoapp.restapi.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.security.PermitAll;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private static ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping()
    public List<UserDto> getAllUsers() {
        List<User> listOfUsers = userService.getAllUsers();
        return convertToDto(listOfUsers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long id) throws UserNotFoundException {
        Optional<User> user = userService.getUserById(id);
        if (!user.isPresent()) {
            throw new UserNotFoundException(id);
        }
        return new ResponseEntity<>(convertToDto(user), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto dto, UriComponentsBuilder ucBuilder) {
        User user = userService.createUser(convertToEntity(dto));

        return new ResponseEntity<>(convertToDto(user), HttpStatus.CREATED);

    }


    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto dto, @PathVariable Long id) throws UserNotFoundException{
        Optional<User> user = userService.getUserById(id);
        if (!user.isPresent()) {
            throw new UserNotFoundException(id);
        }

        User updatedUser = userService.updateUser(convertToEntity(dto));
        return new ResponseEntity<>(convertToDto(updatedUser),HttpStatus.NO_CONTENT);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable Long id) throws UserNotFoundException {
        Optional<User> user = userService.getUserById(id);
        if (!user.isPresent()) {
            throw new UserNotFoundException(id);
        }
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }




    private UserDto convertToDto(Optional<User> user) {
        return user.map(this::convertToDto).orElse(null);
    }

    private UserDto convertToDto(User user) {

        return modelMapper.map(user, UserDto.class);
    }

    private List<UserDto> convertToDto(List<User> users) {
        List<UserDto> userDtos = new ArrayList<>();
        for (User user : users) {
            userDtos.add(this.convertToDto(user));
        }
        return userDtos;
    }

    private User convertToEntity(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }


}

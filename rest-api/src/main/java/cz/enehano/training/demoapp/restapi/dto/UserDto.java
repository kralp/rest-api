package cz.enehano.training.demoapp.restapi.dto;

import lombok.Data;

@Data
public class UserDto {
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNr;
    private String password;
}
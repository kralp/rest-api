package cz.enehano.training.demoapp.restapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import cz.enehano.training.demoapp.restapi.RestApiApplication;
import cz.enehano.training.demoapp.restapi.bootstrap.DataLoader;
import cz.enehano.training.demoapp.restapi.dao.UserRepository;
import cz.enehano.training.demoapp.restapi.dto.UserDto;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.URL;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.DEFINED_PORT, classes={ RestApiApplication.class })
public class UserControllerTest {

    @Value("${local.server.port}")
    private int port;
    private URL base;
    private TestRestTemplate template;


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DataLoader dataLoader;

    private static final String JSON_CONTENT_TYPE = "application/json;charset=UTF-8";





    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/users");
        template = new TestRestTemplate().withBasicAuth("admin@admin.cz", "password");

        /* remove and reload test data */
        userRepository.deleteAll();
        dataLoader.createUsers();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getAllUsers() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString(), String.class);
        assertEquals(HttpStatus.OK,response.getStatusCode());

        List<UserDto> users = convertJsonToUserDtos(response.getBody());
        assertEquals(4, users.size());

    }

    @Test
    public void getUser() throws Exception {
        Long userId = getUserIdByEmail("admin@admin.cz");
        ResponseEntity<String> response = template.getForEntity(String.format("%s/%s", base.toString(), userId), String.class);

        assertEquals(HttpStatus.OK,response.getStatusCode());

        UserDto user = convertJsonToUserDto(response.getBody());
        assertEquals("admin", user.getFirstName());
        assertEquals("admin", user.getLastName());
        assertEquals("admin@admin.cz", user.getEmail());
        assertEquals("12345677", user.getPhoneNr());

    }

    @Test
    public void createUser() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setFirstName("testPavel");
        userDto.setLastName("testKral");
        userDto.setEmail("testpavel@pkral.cz");
        userDto.setPhoneNr("123456");
        userDto.setPassword("testPassword");


        ResponseEntity<String> response = template.postForEntity(base.toString(), userDto, String.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(response.getHeaders().getContentType().toString(), JSON_CONTENT_TYPE);


        UserDto returnedUserDto = convertJsonToUserDto(response.getBody());
        assertEquals(returnedUserDto.getFirstName(),userDto.getFirstName());
        assertEquals(returnedUserDto.getLastName(),userDto.getLastName());
        assertEquals(returnedUserDto.getEmail(),userDto.getEmail());
        assertEquals(returnedUserDto.getPhoneNr(),userDto.getPhoneNr());


    }

    @Test
    public void updateUser() throws Exception {
        Long userId = getUserIdByEmail("admin@admin.cz");
        ResponseEntity<String> response = template.getForEntity(String.format("%s/%s", base.toString(), userId), String.class);

        assertEquals(HttpStatus.OK,response.getStatusCode());

        UserDto user = convertJsonToUserDto(response.getBody());
        assertEquals("admin", user.getFirstName());
        assertEquals("admin", user.getLastName());
        assertEquals("admin@admin.cz", user.getEmail());
        assertEquals("12345677", user.getPhoneNr());

        user.setFirstName("xxxAdmin");
        user.setLastName("xxxAdmin");

        /* PUT updated customer */
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<UserDto> entity = new HttpEntity<>(user, headers);
        ResponseEntity<String> response2 = template.exchange(String.format("%s/%s", base.toString(), userId), HttpMethod.PUT, entity, String.class, userId);
        assertEquals(HttpStatus.NO_CONTENT, response2.getStatusCode());


        /* GET updated customer and ensure name is updated as expected */
        ResponseEntity<String> updatedUserResponse = template.getForEntity(String.format("%s/%s", base.toString(), userId), String.class);
        assertEquals(HttpStatus.OK, updatedUserResponse.getStatusCode());
        assertEquals(JSON_CONTENT_TYPE, updatedUserResponse.getHeaders().getContentType().toString());

        UserDto updatedUser = convertJsonToUserDto(updatedUserResponse.getBody());
        assertEquals("xxxAdmin", updatedUser.getFirstName());
        assertEquals("xxxAdmin", updatedUser.getLastName());
        assertEquals("admin@admin.cz", updatedUser.getEmail());
        assertEquals("12345677", updatedUser.getPhoneNr());



    }

    @Test
    public void deleteUser() throws Exception {
        Long userId = getUserIdByEmail("user@user.cz");
        ResponseEntity<String> response = template.getForEntity(String.format("%s/%s", base.toString(), userId), String.class);
        assertEquals(HttpStatus.OK,response.getStatusCode());

        UserDto user = convertJsonToUserDto(response.getBody());
        assertNotNull(user);

        template.delete(String.format("%s/%s", base.toString(), userId), String.class);

        ResponseEntity<String> response2 = template.getForEntity(String.format("%s/%s", base.toString(), userId), String.class);
        assertEquals(HttpStatus.NOT_FOUND, response2.getStatusCode());
    }


    private UserDto convertJsonToUserDto(String json) throws Exception{
        return objectMapper.readValue(json, UserDto.class);
    }

    private List<UserDto> convertJsonToUserDtos(String json) throws Exception {

        return objectMapper.readValue(json, TypeFactory.defaultInstance().constructCollectionType(List.class, UserDto.class));
    }

    private Long getUserIdByEmail(String email) {
        return userRepository.findByEmail(email).get().getId();
    }

}